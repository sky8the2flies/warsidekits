package com.sky8the2flies.kits;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryClickListener implements Listener {
	
	@EventHandler
	public void invClick(final InventoryClickEvent event) {
		try {
			Kit kit = KitManager.getKit((Player) event.getWhoClicked());
			if (event.getCurrentItem().getType() == Material.AIR)
				return;
			if (event.getInventory().getName().contains(ChatColor.stripColor(Inv.MAIN))) {
				event.setCancelled(true);
				if (event.getCurrentItem().getType() == Material.DIAMOND_SWORD) {
					Inv.openWeapon((Player)event.getWhoClicked());
				} else if (event.getCurrentItem().getType() == Material.DIAMOND_CHESTPLATE) {
					Inv.openArmor((Player)event.getWhoClicked());
				} else if (event.getCurrentItem().getType() == Material.COOKED_BEEF) {
					Inv.openFood((Player)event.getWhoClicked());
				}else if (event.getCurrentItem().getType() == Material.LAVA_BUCKET) {
					Inv.openUtil((Player)event.getWhoClicked());
				} else if (event.getCurrentItem().getType() == Material.REDSTONE) {
					Inv.openItems(kit);
				} else if (event.getCurrentItem().getType() == Material.CLAY_BALL) {
					Inv.openMain(kit.getPlayer(), kit);
				}
			} else if (event.getInventory().getName().contains(ChatColor.stripColor(Inv.WEAPON))) {
				event.setCancelled(true);
				ItemStack is = Inv.createItemStack(new ItemStack(event.getCurrentItem().getType(), event.getCurrentItem().getAmount(), event.getCurrentItem().getDurability()), ChatColor.translateAlternateColorCodes('&', event.getCurrentItem().getItemMeta().getDisplayName()));
				String hashcode = is.hashCode() + ":" + (event.getSlot() + 1);
				if (Inv.isItem(hashcode, "weapon")) {
					if (kit.getHashCodes().contains(hashcode)) {
						for (String items : Kits.instance.getConfig().getConfigurationSection("weapon").getKeys(false)) 
							if (Kits.instance.getConfig().getString(("weapon." + items + ".hashcode")).equals(hashcode)) 
								if (Kits.instance.getPlayerPoints().getAPI().take(kit.getPlayer().getUniqueId(), Kits.instance.getConfig().getInt("weapon." + items + ".price"))) {
									System.out.println("s");
									Kits.instance.getPlayerPoints().getAPI().take(kit.getPlayer().getUniqueId(), Kits.instance.getConfig().getInt("weapon." + items + ".price"));
									Inv.calulatePoints(kit, -Kits.instance.getConfig().getInt("weapon." + items + ".price"));
								} else {
									kit.getPlayer().sendMessage("No PlayerPoints");
									return;
								}
						kit.getContents().remove(is);
						kit.getHashCodes().remove(hashcode);
						Inv.openWeapon((Player) event.getWhoClicked());
					} else {
						if (!Kits.instance.getConfig().getStringList("enchant." + event.getCurrentItem().getTypeId()).isEmpty()) {
							Inv.openEnchant((Player) event.getWhoClicked(), is);
						} else {
							for (String items : Kits.instance.getConfig().getConfigurationSection("weapon").getKeys(false)) {
								if (Kits.instance.getConfig().getString(("weapon." + items + ".hashcode")).equals(hashcode))  {
									if (Kits.instance.getPlayerPoints().getAPI().give(kit.getPlayer().getUniqueId(), Kits.instance.getConfig().getInt("weapon." + items + ".price")))
										Inv.calulatePoints(kit, Kits.instance.getConfig().getInt("weapon." + items + ".price"));
								}
							}
							kit.getContents().add(is);
							kit.getHashCodes().add(hashcode);
							Inv.openWeapon((Player) event.getWhoClicked());
						}
						
					}
				}
			} else if (event.getInventory().getName().contains(ChatColor.stripColor(Inv.ARMOR))) {
				event.setCancelled(true);
				ItemStack is = Inv.createItemStack(new ItemStack(event.getCurrentItem().getType(), event.getCurrentItem().getAmount(), event.getCurrentItem().getDurability()), ChatColor.translateAlternateColorCodes('&', event.getCurrentItem().getItemMeta().getDisplayName()));
				String hashcode = is.hashCode() + ":" + (event.getSlot() + 1);
				if (Inv.isItem(hashcode, "armor")) {
					if (kit.getHashCodes().contains(hashcode)) {
						for (String items : Kits.instance.getConfig().getConfigurationSection("armor").getKeys(false)) 
							if (Kits.instance.getConfig().getString(("armor." + items + ".hashcode")).equals(hashcode)) {
								Kits.instance.getPlayerPoints().getAPI().give(kit.getPlayer().getUniqueId(), Kits.instance.getConfig().getInt("weapon." + items + ".price"));
								Inv.calulatePoints(kit, -Kits.instance.getConfig().getInt("armor." + items + ".price"));
							}
						kit.getContents().remove(is);
						kit.getHashCodes().remove(hashcode);
						Inv.openArmor((Player) event.getWhoClicked());
					} else {
						if (!Kits.instance.getConfig().getStringList("enchant." + event.getCurrentItem().getTypeId()).isEmpty()) {
							Inv.openEnchant((Player) event.getWhoClicked(), is);
						} else {
							for (String items : Kits.instance.getConfig().getConfigurationSection("armor").getKeys(false)) {
								if (Kits.instance.getConfig().getString(("armor." + items + ".hashcode")).equals(hashcode))  
									if (Kits.instance.getPlayerPoints().getAPI().take(kit.getPlayer().getUniqueId(), Kits.instance.getConfig().getInt("weapon." + items + ".price"))) {
										Inv.calulatePoints(kit, Kits.instance.getConfig().getInt("armor." + items + ".price"));
									}else {
										kit.getPlayer().sendMessage("No PlayerPoints");
										return;
									}
								
							}
							kit.getContents().add(is);
							kit.getHashCodes().add(hashcode);
							Inv.openArmor((Player) event.getWhoClicked());
						}
					}
				}
			} else if (event.getInventory().getName().contains(ChatColor.stripColor(Inv.FOOD))) {
				event.setCancelled(true);
				ItemStack is = Inv.createItemStack(new ItemStack(event.getCurrentItem().getType(), event.getCurrentItem().getAmount(), event.getCurrentItem().getDurability()), ChatColor.translateAlternateColorCodes('&', event.getCurrentItem().getItemMeta().getDisplayName()));
				String hashcode = is.hashCode() + ":" + (event.getSlot() + 1);
				if (Inv.isItem(hashcode, "food")) {
					for (String items : Kits.instance.getConfig().getConfigurationSection("food").getKeys(false)) {
						if (Kits.instance.getConfig().getString(("food." + items + ".hashcode")).equals(hashcode))  {
							if (kit.getHashCodes().contains(is.hashCode() + ":" + (event.getSlot() + 1)))
								Inv.calulatePoints(kit, -Kits.instance.getConfig().getInt("food." + items + ".price"));
							else Inv.calulatePoints(kit, Kits.instance.getConfig().getInt("food." + items + ".price"));		
						}
					}
					if (kit.getHashCodes().contains(hashcode)) {
						kit.getContents().remove(is);
						kit.getHashCodes().remove(hashcode);
						Inv.openFood((Player) event.getWhoClicked());
					} else {
						kit.getContents().add(is);
						kit.getHashCodes().add(hashcode);
						Inv.openFood((Player) event.getWhoClicked());
					}
				}
			} else if (event.getInventory().getName().contains(ChatColor.stripColor(Inv.UTILITIES))) {
				event.setCancelled(true);
				ItemStack is = Inv.createItemStack(new ItemStack(event.getCurrentItem().getType(), event.getCurrentItem().getAmount(), event.getCurrentItem().getDurability()), ChatColor.translateAlternateColorCodes('&', event.getCurrentItem().getItemMeta().getDisplayName()));
				String hashcode = is.hashCode() + ":" + (event.getSlot() + 1);
				if (Inv.isItem(hashcode, "util")) {
					for (String items : Kits.instance.getConfig().getConfigurationSection("util").getKeys(false)) {
						if (Kits.instance.getConfig().getString(("util." + items + ".hashcode")).equals(hashcode))  {
							if (kit.getHashCodes().contains(is.hashCode() + ":" + (event.getSlot() + 1)))
								Inv.calulatePoints(kit, -Kits.instance.getConfig().getInt("util." + items + ".price"));
							else Inv.calulatePoints(kit, Kits.instance.getConfig().getInt("util." + items + ".price"));		
						}
					}
					if (kit.getHashCodes().contains(hashcode)) {
						kit.getContents().remove(is);
						kit.getHashCodes().remove(hashcode);
						Inv.openUtil((Player) event.getWhoClicked());
					} else {
						kit.getContents().add(is);
						kit.getHashCodes().add(hashcode);
						Inv.openUtil((Player) event.getWhoClicked());
					}
				}
			} else if (event.getInventory().getName().contains(ChatColor.stripColor(Inv.ENCHANT))) {
				event.setCancelled(true);
				String[] split = event.getInventory().getItem(13).getItemMeta().getDisplayName().split(":");
				if (event.getCurrentItem().getType() == Material.ARROW) {
					if (event.getInventory().getItem(13).getEnchantments().size() < 1) {
						kit.getPlayer().sendMessage("Please pick an enchantment below!");
					} else {
						Map<Enchantment, Integer> map = event.getInventory().getItem(13).getEnchantments();
						int id = 0;
						Enchantment ench = null;
						for (Entry<Enchantment, Integer> entry : map.entrySet()) {
						    id = entry.getValue();
						    ench = entry.getKey();
						}
						ItemMeta im = event.getInventory().getItem(13).getItemMeta();
						List<String> lore = new ArrayList<String>();
						if (lore.contains("Capacity:")) {
							int number = Integer.parseInt(lore.get(0).replace("Price: ", "")) + ((id + Integer.parseInt(event.getCurrentItem().getItemMeta().getDisplayName())) * Integer.parseInt(split[3]));
							lore.add("Capacity: " + (number < 0 ? -number : number));
						} else {
							int number = (id + Integer.parseInt(event.getCurrentItem().getItemMeta().getDisplayName())) * Integer.parseInt(split[3]);
							lore.add("Capacity: " + (number < 0 ?  -number : number));
						}
						im.setLore(lore);
						event.getInventory().getItem(13).setItemMeta(im);
						if (!((id + Integer.parseInt(event.getCurrentItem().getItemMeta().getDisplayName())) < 1) && !((id + Integer.parseInt(event.getCurrentItem().getItemMeta().getDisplayName())) > Integer.parseInt(split[2]))) {
							event.getInventory().getItem(13).removeEnchantment(ench);
							event.getInventory().getItem(13).addUnsafeEnchantment(ench, id + Integer.parseInt(event.getCurrentItem().getItemMeta().getDisplayName()));
						} else {
							kit.getPlayer().sendMessage("Enchantments don't go that low/far!");
						}
						
					}
				} else if (event.getCurrentItem().getType() == Material.BOOK) {
					if (!event.getCurrentItem().getItemMeta().getDisplayName().contains("Current Enchantment:")) {
						if (!split[1].equalsIgnoreCase(event.getCurrentItem().getItemMeta().getDisplayName().split(":")[0])) {
							event.getInventory().setItem(13, Inv.createItemStack(new ItemStack(Material.BOOK), "Current Enchantment: : :"));
							event.getInventory().getItem(13).addUnsafeEnchantment(Enchantment.getByName(event.getCurrentItem().getItemMeta().getDisplayName().split(":")[0]), 1);
							ItemMeta im = event.getInventory().getItem(13).getItemMeta();
							im.setDisplayName("Current Enchantment:" + event.getCurrentItem().getItemMeta().getDisplayName());
							event.getInventory().getItem(13).setItemMeta(im);
						} else
							kit.getPlayer().sendMessage("You are already using that enchantment!");
					}
				} else if (event.getCurrentItem().getType() == Material.WOOL) {
					event.getInventory().getItem(4).addUnsafeEnchantments(event.getInventory().getItem(13).getEnchantments());
				} else if (event.getCurrentItem().getType() == Material.EMERALD) {
					Map<Enchantment, Integer> map = event.getInventory().getItem(4).getEnchantments();
					int id = 0;
					Enchantment ench = null;
					for (Entry<Enchantment, Integer> entry : map.entrySet()) {
					    id = entry.getValue();
					    ench = entry.getKey();
					    for (int i = 18; event.getInventory().getItem(i).getType() != Material.AIR; i++) {
					    	String[] isplit = event.getInventory().getItem(i).getItemMeta().getDisplayName().split(":");
					    	if (isplit[0].equals(ench.getName())) {
					    		int amount = id * Integer.parseInt(isplit[2]);
					    		if (Kits.instance.getPlayerPoints().getAPI().take(kit.getPlayer().getUniqueId(), amount)) {
									Inv.calulatePoints(kit, amount);
									kit.getContents().add(event.getInventory().getItem(4));
								}else {
									kit.getPlayer().sendMessage("No PlayerPoints");
									return;
								}
					    	}
					    }
					}
				}
			} else if (event.getInventory().getName().contains("Kit:")) {
				event.setCancelled(true);
			}
			FileConfiguration config = Kits.instance.getStorage().getConfig();
			config.set("storage." + kit.getPlayer().getUniqueId() + ".maxPoints", kit.getMax());
			config.set("storage." + kit.getPlayer().getUniqueId() + ".curPoints", kit.getCurrent());
			config.set("storage." + kit.getPlayer().getUniqueId()+ ".contents", null);
			for (int i = 0; i < kit.getContents().size(); i++) {
				config.set("storage." + kit.getPlayer().getUniqueId()+ ".contents." + i, kit.getContents().get(i));
			}
			Kits.instance.getStorage().saveConfig();
		} catch(NullPointerException e) {
			if (event.getInventory().getType() == InventoryType.PLAYER)
				return;
			if (event.getInventory().getName().contains(Inv.WEAPON) || event.getInventory().getName().contains(Inv.ARMOR) || event.getInventory().getName().contains(Inv.FOOD) || event.getInventory().getName().contains(Inv.UTILITIES) || event.getInventory().getName().contains(Inv.ENCHANT) ||event.getInventory().getName().contains("Kit:"))
				Inv.openMain((Player)event.getWhoClicked(), KitManager.getKit((Player)event.getWhoClicked()));
		}
	}
}
