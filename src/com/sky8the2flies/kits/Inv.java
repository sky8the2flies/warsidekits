package com.sky8the2flies.kits;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Inv {
	
	public static String MAIN = ChatColor.WHITE + "Kit Selection:";
	public static String WEAPON = ChatColor.WHITE + "Weapon Selection:";
	public static String ARMOR = ChatColor.WHITE + "Armor Selection:";
	public static String FOOD = ChatColor.WHITE + "Food Selection:";
	public static String UTILITIES = ChatColor.WHITE + "Util Selection:";
	public static String ENCHANT = ChatColor.WHITE + "Enchant:";
	
	public static void openMain(Player player, Kit kit) {
		Inventory inv = Kits.instance.getServer().createInventory(null, 9, MAIN + " " + kit.getPoints());
		inv.setItem(1, createItemStack(new ItemStack(Material.DIAMOND_SWORD), "&e&lChoose your weapon!", " ", " &7- This contains your basic weapons,", "&r   &7including a sword and or a axe!"));
		inv.setItem(2, createItemStack(new ItemStack(Material.DIAMOND_CHESTPLATE), "&6&lChoose your armor!", " ", " &7- This contains your basic armor,", "&r    &7including a chestplate and or leggings!"));
		inv.setItem(3, createItemStack(new ItemStack(Material.COOKED_BEEF), "&a&lChoose your diet!", " ", " &7- This contains your basic food items."));
		inv.setItem(4, createItemStack(new ItemStack(Material.LAVA_BUCKET), "&c&lChoose your style!", " ", " &7- This contains your basic fighting elements,", "&r    &7including lava buckets etc..."));
		inv.setItem(5, createItemStack(new ItemStack(Material.CLAY_BALL), "&b&lChoose your cooldown!", " ", "&7- This contains the cooldowns you can have for the kit."));
		inv.setItem(7, createItemStack(new ItemStack(Material.REDSTONE), "&2&lView your kit!", " ", "&7- This contains your kit items!"));
		inv.setItem(8, createItemStack(new ItemStack(Material.WOOL, 1, (short) 5), "&2&lApply your kit!", " ", "&7- This contains your kit items!"));
		player.openInventory(inv);
	}
	
	public static void openWeapon(Player player) {
		Inventory inv = Kits.instance.getServer().createInventory(null, 9 * 5, WEAPON + " " + KitManager.getKit(player).getPoints());
		List<ItemStack> parsed = parseItems("weapon", player);
		for (int i = 0; i < parsed.size(); i++) {
			inv.setItem(i, parsed.get(i));
		}
		player.openInventory(inv);	
	}
	
	public static void openArmor(Player player) {
		Inventory inv = Kits.instance.getServer().createInventory(null, 9* 5, ARMOR + " " + KitManager.getKit(player).getPoints());
		List<ItemStack> parsed = parseItems("armor", player);
		for (int i = 0; i < parsed.size(); i++) {
			inv.setItem(i, parsed.get(i));
		}
		player.openInventory(inv);	
	}
	
	public static void openFood(Player player) {
		Inventory inv = Kits.instance.getServer().createInventory(null, 9* 5, FOOD + " " + KitManager.getKit(player).getPoints());
		List<ItemStack> parsed = parseItems("food", player);
		for (int i = 0; i < parsed.size(); i++) {
			inv.setItem(i, parsed.get(i));
		}
		player.openInventory(inv);	
	}
	
	public static void openUtil(Player player) {
		Inventory inv = Kits.instance.getServer().createInventory(null, 9* 5, UTILITIES + " " + KitManager.getKit(player).getPoints());
		List<ItemStack> parsed = parseItems("util", player);
		for (int i = 0; i < parsed.size(); i++) {
			inv.setItem(i, parsed.get(i));
		}
		player.openInventory(inv);	
	}
	
	public static void openEnchant(Player player, ItemStack is) {
		Inventory inv = Kits.instance.getServer().createInventory(null, 9* 5, ENCHANT + " " + KitManager.getKit(player).getPoints());
		inv.setItem(4, is);
		inv.setItem(5, createItemStack(new ItemStack(Material.WOOL), "Apply enchantment, continue editing kit."));
		inv.setItem(6, createItemStack(new ItemStack(Material.EMERALD), "Apply enchanted item to kit, return to main inventory."));
		inv.setItem(11, createItemStack(new ItemStack(Material.ARROW), "-5"));
		inv.setItem(12, createItemStack(new ItemStack(Material.ARROW), "-1"));
		inv.setItem(13, createItemStack(new ItemStack(Material.BOOK), "Current Enchantment: : :"));
		inv.setItem(14, createItemStack(new ItemStack(Material.ARROW), "+1"));
		inv.setItem(15, createItemStack(new ItemStack(Material.ARROW), "+5"));
		List<String> enchants = Kits.instance.getConfig().getStringList("enchant." + is.getTypeId());
		int i = 0;
		for (String str : enchants) {
			String[] split = str.split(":");
			ItemStack ench = createItemStack(new ItemStack(Material.BOOK), split[0] + ":" + split[1] + ":" + split[2]);
			inv.setItem(i + 18, ench);
			i++;
		}
		i = 0;
		player.openInventory(inv);
	}
	
	public static void openItems(Kit kit) {
		Inventory inv = Kits.instance.getServer().createInventory(null, 54, ChatColor.WHITE + "Kit:");
		for (ItemStack stack : kit.getContents()) {
			inv.addItem(stack);
		}
		kit.getPlayer().openInventory(inv);
	}
	
	public static List<ItemStack> parseItems(String query, Player player) {
		FileConfiguration config = Kits.instance.getConfig();
		List<ItemStack> items = new ArrayList<ItemStack>();
		for (String item : config.getConfigurationSection(query).getKeys(false)) {
			String base = query + "." + item + ".";
			String name = config.getString(base + "name");
			String amount = config.getString(base + "amount");
			String id = config.getString(base + "id");
			String meta = config.getString(base + "meta");
			int price = config.getInt(base + "price");
			ItemStack is = createItemStack(new ItemStack(Material.getMaterial(Integer.parseInt(id)), Integer.parseInt(amount), Short.parseShort(meta)), ChatColor.translateAlternateColorCodes('&', name));
			if (KitManager.getKit(player).getContents().contains(is))
				if (!KitManager.getKit(player).getHashCodes().contains(is.hashCode() + ":" + (items.size() + 1)))
					KitManager.getKit(player).getHashCodes().add(is.hashCode() + ":" + (items.size() + 1));
			config.set(base + "hashcode", is.hashCode() + ":" + (items.size() + 1));
			ItemMeta im = is.getItemMeta();
			List<String> lore = new ArrayList<String>();
			if (!KitManager.getKit(player).getHashCodes().contains(is.hashCode() + ":" + (items.size() + 1))) {
				lore.add(ChatColor.DARK_GREEN + ChatColor.BOLD.toString() + "> Capacity: " + String.valueOf(price).replace("-", ""));
			} else {
				lore.add(ChatColor.RED + ChatColor.BOLD.toString() + "> Capacity: -" + String.valueOf(price).replace("-", ""));
				lore.add(ChatColor.GREEN + ChatColor.BOLD.toString() + "SELECTED");
			}
			im.setLore(lore);
			is.setItemMeta(im);
			items.add(is);
			Kits.instance.saveConfig();
		}
		return items;
	}
	
	public static void calulatePoints(Kit kit, int points) {
		kit.setCurrent(kit.getCurrent() + points);
	}
	
	public static boolean isItem(String hashcode, String query) {
		for (String items : Kits.instance.getConfig().getConfigurationSection(query).getKeys(false)) {
			if (Kits.instance.getConfig().getString((query + "." + items + ".hashcode")).equals(hashcode))  {
				return true;		
			}
		}
		return false;
	}
	
	public static ItemStack createItemStack(ItemStack stack, String name, String... lores) {
		ItemMeta meta = stack.getItemMeta();

		meta.setDisplayName(format(name));

		List<String> loreList = new ArrayList<String>();

		for (String lore : lores) {
			loreList.add(format(lore));
		}

		meta.setLore(loreList);
		
		stack.setItemMeta(meta);
		return stack;
	}
	
	public static ItemStack createEnchantedItemStack(ItemStack stack, Map<Enchantment,Integer> ench, String name, String... lores) {
		ItemMeta meta = stack.getItemMeta();

		meta.setDisplayName(format(name));

		List<String> loreList = new ArrayList<String>();

		for (String lore : lores) {
			loreList.add(format(lore));
		}

		meta.setLore(loreList);
		
		stack.setItemMeta(meta);
		
		stack.addUnsafeEnchantments(ench);
		
		return stack;
	}

	public static String format(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}

}
