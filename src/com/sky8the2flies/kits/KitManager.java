package com.sky8the2flies.kits;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class KitManager {
	
	private static List<Kit> kits = new ArrayList<Kit>();
	
	public static Kit addKit(Player player) {
		Kit newKit = null;
		if (Kits.instance.getStorage().getConfig().isConfigurationSection("storage." + player.getUniqueId())) {
			newKit = new Kit(player, Kits.instance.getStorage().getConfig().getInt("storage." + player.getUniqueId() + ".maxPoints"), Kits.instance.getStorage().getConfig().getInt("storage." + player.getUniqueId() + ".curPoints"));
			List<ItemStack> contents = new ArrayList<ItemStack>();
			if (Kits.instance.getStorage().getConfig().isConfigurationSection("storage." + player.getUniqueId() + ".contents")) {
				for (int i = 0; i < Kits.instance.getStorage().getConfig().getConfigurationSection("storage." + player.getUniqueId() + ".contents").getKeys(false).size(); i++) {
					contents.add(Kits.instance.getStorage().getConfig().getItemStack("storage." + player.getUniqueId() + ".contents." + i));
				}
			}
			newKit.setContents(contents);
		} else 
			newKit = new Kit(player, Kits.instance.getConfig().getInt("kit.defaultCoins"), 0);
		kits.add(newKit);
		return newKit;
	}
	
	public static Kit getKit(Player player) {
		for (Kit kit : kits) {
			if (kit.getPlayer().getUniqueId().equals(player.getUniqueId())) {
				return kit;
			}
		}
		return addKit(player);
	}
	
}
