package com.sky8the2flies.kits;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KitCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			Lang.sendMessage(sender, Lang.PREFIX + "&cYou must be a player to execute this command!");
			return false;
		}
		Player player = (Player) sender;
		if (cmd.getName().equalsIgnoreCase("kit")) {
			if (args.length < 1) {
				player.chat("/ekits");
				return false;
			}
			if (args[0].equalsIgnoreCase("custom")) {
				Kit kit = KitManager.getKit(player);
				Inv.openMain(player, kit);
			} else
				player.chat("/ekit " + args[0]);
		}
		return false;
	}

}
