package com.sky8the2flies.kits;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Kit {
	
	private int max, current;
	private Player player;
	private List<ItemStack> contents = new ArrayList<ItemStack>();
	private List<String> hashCodes = new ArrayList<String>();
	private ItemStack currentEnchant;
	
	public Kit(Player player, int maxPoints, int currentPoints) {
		this.setPlayer(player);
		this.setMax(maxPoints);
		this.setCurrent(currentPoints);
	}
	
	public boolean canDo(int points) {
		if (getPoints() - points < 0) {
			player.sendMessage("That transaction is to much for the current item!");
			return false;
		}		
		return true;
	}
	
	public int getPoints() {
		if (this.getCurrent() > this.getMax()) {
			return 0;
		}
		return this.getMax() - this.getCurrent();
	}

	public int getCurrent() {
		return current;
	}

	public void setCurrent(int current) {
		this.current = current;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public List<ItemStack> getContents() {
		return contents;
	}

	public void setContents(List<ItemStack> contents) {
		this.contents = contents;
	}

	public List<String> getHashCodes() {
		return hashCodes;
	}

	public void setHashCodes(List<String> hashCodes) {
		this.hashCodes = hashCodes;
	}

	public ItemStack getCurrentEnchant() {
		return currentEnchant;
	}

	public void setCurrentEnchant(ItemStack currentEnchant) {
		this.currentEnchant = currentEnchant;
	}

}
