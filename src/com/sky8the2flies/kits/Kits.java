package com.sky8the2flies.kits;

import org.black_ixx.playerpoints.PlayerPoints;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Kits extends JavaPlugin {
	
	public static Kits instance;
	private PlayerPoints playerPoints;
	private ConfigAccessor cfgA;
	
	public void onEnable() {
		super.onEnable();
		if (!hookPlayerPoints())
			return;
		instance = this;
		saveConfig();
		cfgA = new ConfigAccessor(this, "storage.yml");
		getServer().getPluginManager().registerEvents(new InventoryClickListener(), this);
		getCommand("kit").setExecutor(new KitCommand());
	}
	
	public ConfigAccessor getStorage() {
		return cfgA;
	}
	
	private boolean hookPlayerPoints() {
	    final Plugin plugin = this.getServer().getPluginManager().getPlugin("PlayerPoints");
	    playerPoints = PlayerPoints.class.cast(plugin);
	    return playerPoints != null; 
	}
	
	public PlayerPoints getPlayerPoints() {
	    return playerPoints;
	}

}
